﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpicBattleship
{
    class ShipSetup
    {
        public List<Ship> Setup { get; set; }

        public ShipSetup(List<Ship> laivai)
        {
            this.Setup = laivai;
        }
    }

    public class Ship
    {
        public Kryptis Kryptis { get; set; }
        public Forma Forma { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Koordinates { get; set; }
    }

    public enum Kryptis
    {
        D,
        Z
    }

    public enum Forma
    {
        F1 = 1,
        F2 = 2,
        F3 = 3,
        F4 = 4
    }
}
