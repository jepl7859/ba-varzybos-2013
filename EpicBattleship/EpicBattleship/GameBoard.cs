﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EpicBattleship
{
    public class GameBoard
    {
        private const int Lenght = 10;
        public Dictionary<string, Cell> Sea;
        public List<Ship> Ships;

        public override string ToString()
        {
            var result = string.Empty;

            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    result += string.Format("{0:00} ", GetCell(j, i).Value);
                }
                result += "\n";
            }
            return result;
        }

        public void InitSea()
        {
            Sea = new Dictionary<string, Cell>();
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    var cell = new Cell(i, j);
                    Sea.Add(cell.Key, new Cell(i, j));
                }
            }
        }

        public string ConvertToKoordinates(int x, int y)
        {
            var cell = new Cell(x, y);
            return cell.Key;
        }

        public int NextToHit(Cell cell)
        {
            var result = 0;

            const int modifier = 40;
            //left
            try
            {
                if (cell.X > 1 && Sea[ConvertToKoordinates(cell.X - 1, cell.Y)].State == CellState.Hit)
                {
                    result += modifier;
                }

            }
            catch { }

            // right
            try
            {
                if (cell.X < 10 && Sea[ConvertToKoordinates(cell.X + 1, cell.Y)].State == CellState.Hit)
                {
                    result += modifier;
                }
            }
            catch { }
            // up
            try
            {
                if (cell.Y > 1 && Sea[ConvertToKoordinates(cell.X, cell.Y - 1)].State == CellState.Hit)
                {
                    result += modifier;
                }
            }
            catch { }
            // down
            try
            {
                if (cell.Y < 10 && Sea[ConvertToKoordinates(cell.X, cell.Y + 1)].State == CellState.Hit)
                {
                    result += modifier;
                }
            }
            catch { }
            return result;
        }

        public void CalculateValues()
        {

            foreach (var cell in Sea.Values)
            {
                cell.Value = 0;
            }

            foreach (var cell in Sea.Values)
            {
                try
                {
                    foreach (var ship in Ships)
                    {
                        var length = (int)ship.Forma;
                        //horizontal
                        if (CheckIfFits(cell.X, cell.Y, Kryptis.D, ship.Forma, ConvertToKoordinates(cell.X, cell.Y)))
                        {
                            for (int i = 0; i < length; i++)
                            {
                                try
                                {
                                    Sea[ConvertToKoordinates(cell.X + i, cell.Y)].Value += 1;
                                }
                                catch
                                {
                                }
                            }
                        }
                        //vertical
                        if (ship.Forma != Forma.F1 && CheckIfFits(cell.X, cell.Y, Kryptis.Z, ship.Forma, ConvertToKoordinates(cell.X, cell.Y)))
                        {
                            for (int i = 0; i < length; i++)
                            {
                                try
                                {
                                    Sea[ConvertToKoordinates(cell.X, cell.Y + i)].Value += 1;
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                    cell.Value += NextToHit(cell);
                    if (cell.State != CellState.Clear)
                    {
                        cell.Value = 0;
                    }
                }
                catch
                {
                    cell.Value = 0;
                }
            }
        }

        public Cell GetCellWithBestProbability()
        {
            var bestCell = Sea.Values.First();
            var maxProbability = bestCell.Value;

            foreach (var cell in Sea.Values)
            {
                if (cell.State == CellState.Clear && cell.Value > maxProbability)
                {
                    maxProbability = cell.Value;
                    //bestCell = cell;
                }
            }

            var cells = Sea.Values.Where(x => x.Value == maxProbability && x.State == CellState.Clear).ToList();

            var random = new Random();
            if (cells.Any())
            {
                var randomIndex = random.Next(0, cells.Count());
                bestCell = cells[randomIndex];
            }
            else
            {
                bestCell = GetRandomCell();
            }

            return bestCell;
        }

        public string InitShips()
        {
            //var shipSetup1 = new ShipSetup();
            var shipSetup1 = new ShipSetup(
                 new List<Ship>()
                {
                    new Ship() { Forma = Forma.F4, Kryptis = Kryptis.D, Koordinates = "B10", X = 2, Y = 10 },
                    new Ship() { Forma = Forma.F3, Kryptis = Kryptis.Z, Koordinates = "J5", X = 10, Y = 5},
                    new Ship() { Forma = Forma.F3, Kryptis = Kryptis.D, Koordinates = "H9", X = 8, Y = 9},
                    new Ship() { Forma = Forma.F2, Kryptis = Kryptis.Z, Koordinates = "J1", X = 10, Y = 1},
                    new Ship() { Forma = Forma.F2, Kryptis = Kryptis.D, Koordinates = "C1", X = 3, Y = 1},
                    new Ship() { Forma = Forma.F2, Kryptis = Kryptis.Z, Koordinates = "B3", X = 2, Y = 3},
                    new Ship() { Forma = Forma.F1, Kryptis = Kryptis.Z, Koordinates = "C7", X = 3, Y = 7},
                    new Ship() { Forma = Forma.F1, Kryptis = Kryptis.Z, Koordinates = "G6", X = 7, Y = 6},
                    new Ship() { Forma = Forma.F1, Kryptis = Kryptis.Z, Koordinates = "F8", X = 8, Y = 8},
                    new Ship() { Forma = Forma.F1, Kryptis = Kryptis.Z, Koordinates = "F2", X = 8, Y = 2},
                });
            string laivai = "";
            foreach (var ship in shipSetup1.Setup)
            {
                laivai += string.Format("{0} {1} {2}\r\n", ship.Forma.ToString(), ship.Koordinates, ship.Kryptis.ToString());
                string pradineKoordinate = ship.Koordinates.ToUpper();

                for (int k = 0; k < (int)ship.Forma; k++)
                {
                    pradineKoordinate = pradineKoordinate.ToUpper();
                    var cele = Sea[pradineKoordinate];
                    cele.State = CellState.Ship;
                    var tempx = cele.X;
                    var tempy = cele.Y;
                    if (ship.Kryptis == Kryptis.D)
                    {
                        tempx++;
                    }
                    else
                    {
                        tempy++;
                    }
                    if (tempx > 10) break;
                    pradineKoordinate = cele.Letters[tempx] + tempy.ToString();
                }
            }
            return laivai;

        }

        public void InitShips(bool opp)
        {
            //var shipSetup1 = new ShipSetup();
            if (opp)
            {
                var list = new List<Ship>();

                #region standart ship pack
                list.Add(new Ship()
                {
                    Forma = Forma.F1,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });
                list.Add(new Ship()
                {
                    Forma = Forma.F1,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });
                list.Add(new Ship()
                {
                    Forma = Forma.F1,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });
                list.Add(new Ship()
                {
                    Forma = Forma.F1,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });


                //f2
                list.Add(new Ship()
                {
                    Forma = Forma.F2,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });
                list.Add(new Ship()
                {
                    Forma = Forma.F2,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });
                list.Add(new Ship()
                {
                    Forma = Forma.F2,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });

                //3
                list.Add(new Ship()
                {
                    Forma = Forma.F3,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });
                list.Add(new Ship()
                {
                    Forma = Forma.F3,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });

                //4
                list.Add(new Ship()
                {
                    Forma = Forma.F4,
                    X = 1,
                    Y = 1,
                    Koordinates = ConvertToKoordinates(1, 1),
                    Kryptis = Kryptis.D
                });
                #endregion

                Ships = list;
            }
        }

        public bool CheckIfFits(int x, int y, Kryptis kryptis, Forma forma, string koordinates)
        {
            try
            {
                var tempShip = new Ship()
                {
                    X = x,
                    Y = y,
                    Kryptis = kryptis,
                    Forma = forma,
                    Koordinates = koordinates
                };
                return CheckIfFits(tempShip);
            }
            catch
            {
                return false;
            }
        }

        public bool CheckIfFits(Ship ship)
        {
            int length;
            var result = true;

            try
            {
                length = (int)ship.Forma;
            }
            catch
            {
                length = 1;
            }

            var startCell = Sea[ship.Koordinates];
            for (int i = 0; i < length; i++)
            {

                switch (ship.Kryptis)
                {
                    case Kryptis.D:
                        result = result &&
                                 (Sea[ConvertToKoordinates(startCell.X + i, startCell.Y)].State == CellState.Clear ||
                                  Sea[ConvertToKoordinates(startCell.X + i, startCell.Y)].State == CellState.Hit);

                        break;
                    case Kryptis.Z:
                        result = result &&
                                 (Sea[ConvertToKoordinates(startCell.X, startCell.Y + i)].State == CellState.Clear ||
                                  Sea[ConvertToKoordinates(startCell.X, startCell.Y + i)].State == CellState.Hit);
                        break;
                }

                #region +1/-1

                if (result)
                {
                    if (ship.Kryptis == Kryptis.D)
                    {
                        #region D

                        try
                        {
                            result = result &&
                                     (Sea[ConvertToKoordinates(startCell.X + 1, startCell.Y)].State ==
                                      CellState.Clear ||
                                      Sea[ConvertToKoordinates(startCell.X + 1, startCell.Y)].State ==
                                      CellState.Miss);
                        }
                        catch
                        {
                        }
                        if (result)
                        {
                            try
                            {
                                result = result &&
                                         (Sea[ConvertToKoordinates(startCell.X - 1, startCell.Y)].State ==
                                          CellState.Clear ||
                                          Sea[ConvertToKoordinates(startCell.X - 1, startCell.Y)].State ==
                                          CellState.Miss);
                            }
                            catch
                            {
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region Z
                        try
                        {
                            result = result &&
                                     (Sea[ConvertToKoordinates(startCell.X, startCell.Y + 1)].State ==
                                      CellState.Clear ||
                                      Sea[ConvertToKoordinates(startCell.X, startCell.Y + 1)].State ==
                                      CellState.Miss);
                        }
                        catch
                        {
                        }
                        if (result)
                        {
                            try
                            {
                                result = result &&
                                         (Sea[ConvertToKoordinates(startCell.X, startCell.Y - 1)].State ==
                                          CellState.Clear ||
                                          Sea[ConvertToKoordinates(startCell.X, startCell.Y - 1)].State ==
                                          CellState.Miss);
                            }
                            catch
                            {
                            }
                        }
                        #endregion
                    }

                }
                #endregion
            }
            return result;
        }

        public Cell GetCell(int x, int y)
        {
            var cell = new Cell(x, y);
            var key = string.Empty;
            try
            {
                key = cell.Key;
            }
            catch
            {
            }
            return GetCell(key);
        }

        public Cell GetCell(string key)
        {
            try
            {
                return Sea[key];
            }
            catch
            {
                return null;
            }
        }

        public void ResetMissedCells()
        {
            var missedCells = Sea.Values.Where(x => x.State == CellState.Miss);
            foreach (var missedCell in missedCells)
            {
                missedCell.State = CellState.Clear;
            }
        }

        public void MarkCellsNearKill(Cell cell)
        {
            //left
            try
            {
                if (cell.X > 1 && GetCell(cell.X - 1, cell.Y).State != CellState.Hit)
                {
                    GetCell(cell.X - 1, cell.Y).State = CellState.NearHit;
                }
            }
            catch
            {
            }
            // right
            try
            {
                if (cell.X < 10 && GetCell(cell.X + 1, cell.Y).State != CellState.Hit)
                {
                    GetCell(cell.X + 1, cell.Y).State = CellState.NearHit;
                }
            }
            catch
            {
            }

            // up
            try
            {
                if (cell.Y > 1 && GetCell(cell.X, cell.Y - 1).State != CellState.Hit)
                {
                    GetCell(cell.X, cell.Y - 1).State = CellState.NearHit;
                }
            }
            catch
            {
            }
            // down
            try
            {
                if (cell.Y < 10 && GetCell(cell.X, cell.Y + 1).State != CellState.Hit)
                {
                    GetCell(cell.X, cell.Y + 1).State = CellState.NearHit;
                }
            }
            catch 
            {
            }
        }

        public void MarkCornerCellsNearKill(Cell cell)
        {
            try
            {
                var leftTopCornerCell = GetCell(cell.X - 1, cell.Y - 1);
                var leftBottomCornerCell = GetCell(cell.X - 1, cell.Y + 1); 
                var rightTopCornerCell = GetCell(cell.X + 1, cell.Y - 1);
                var rightBottomCornerCell = GetCell(cell.X + 1, cell.Y + 1);


                if (leftTopCornerCell != null )
                {
                    leftTopCornerCell.State = CellState.NearHit;
                }

                if (leftBottomCornerCell != null )
                {
                    leftBottomCornerCell.State = CellState.NearHit;
                }

                if (rightTopCornerCell != null )
                {
                    rightTopCornerCell.State = CellState.NearHit;
                }

                if (rightBottomCornerCell != null )
                {
                    rightBottomCornerCell.State = CellState.NearHit;
                }
            }
            catch
            {
            }
        }

        public void MarkAllAroundDeadShip(List<Cell> cells)
        {
            foreach (var cell in cells)
            {
                MarkCellsNearKill(cell);
                MarkCornerCellsNearKill(cell);
            }
        }

        public void RemoveKilledShip(List<Cell> hits)
        {
            try
            {
                var firstShip = Ships.FirstOrDefault(x => (int) x.Forma == hits.Count);
                Ships.Remove(firstShip);
            }
            catch 
            {
            }
        }

        public Cell GetRandomCell()
        {
            var random = new Random();
            int x = random.Next(0, 9);
            int y = random.Next(0, 9);

            return GetCell(x, y);
        }
    }
}
