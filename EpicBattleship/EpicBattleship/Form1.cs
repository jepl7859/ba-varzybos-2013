﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EpicBattleship
{
    public partial class Form1 : Form
    {
        private GameEngine _engine = new GameEngine();
        public Form1()
        {
            InitializeComponent();
            
            MoveButton.Hide();
            

        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            try
            {
                ShipsPositions.Text = _engine.InitGameBoard();
                ShipsPositions.ReadOnly = true;
                StartButton.Enabled = false;

                MoveButton.Show();
            }
            catch
            {
                OutputBox.Text = "Pasiduodam :(";
            }
        }

        private void MoveButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool error = false;
                string inputText = InputBox.Text;
                if (inputText == "")
                {
                    OutputBox.Text += "\r\nMes sauname i : "+_engine.MakeMove();
                }
                    //pastume
                else if (inputText == "1")
                {
                    _engine.InsertOpponentMove(MoveType.ShipPush, null);
                InputBox.Text = "";
                    OutputBox.Text += "\r\n" + "Mūsų eilė";
                }
                    //pataikem
                else if(inputText == "2")
                {
                    _engine.InsertMoveResults(MoveResult.Hit);
                    OutputBox.Text += "\r\nMes sauname i : " + _engine.MakeMove();
                InputBox.Text = "";
                }
                    //nusove
                else if (inputText == "3")
                {
                    _engine.InsertMoveResults(MoveResult.Dead);
                    OutputBox.Text += "\r\nMes sauname i : " + _engine.MakeMove();
                InputBox.Text = "";
                }
                    //nepataikem
                else if (inputText == "4")
                {
                    _engine.InsertMoveResults(MoveResult.Miss);
                    InputBox.Text = "";
                    OutputBox.Text += "\r\n" + "Priešų eilė";
                }
                    //i mus sauna su koordinate
                else if (inputText.Split(new char[] { ' ' }).Count() == 2)
                {
                    string raide = InputBox.Text.Split(new char[] {' '})[0];
                    string skaicius = InputBox.Text.Split(new char[] { ' ' })[1];
                    if (!Regex.IsMatch(raide, "[a-j]"))
                    {
                        error = true;
                        OutputBox.Text += "\r\nNeatpazinta komanda, vesk dar karta";
                    }
                    int skaiciusNr = 0;
                    if (!(int.TryParse(skaicius, out skaiciusNr) && skaiciusNr <=10 && skaiciusNr > 0))
                    {
                        error = true;
                        OutputBox.Text += "\r\nNeatpazinta komanda, vesk dar karta";
                    }
                    if(!error)
                        OutputBox.Text += "\r\n" + raide+skaicius;
                    _engine.InsertOpponentMove(MoveType.Shoot, raide+skaicius);
                }
                else 
                {
                    error = true;
                    OutputBox.Text += "\r\nNeatpazinta komanda, vesk dar karta";
                }
                if (!error) InputBox.Text = "";
            }
            catch
            {
                OutputBox.Text += "\r\n"+new Random().Next(11).ToString() + new Random().Next(11).ToString();
            }
            OutputBox.SelectionStart = OutputBox.Text.Length;
            OutputBox.ScrollToCaret();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

    }
}
