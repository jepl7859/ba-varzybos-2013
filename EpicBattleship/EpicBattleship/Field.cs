﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpicBattleship
{
    class Field
    {
        private const int Lenght = 10;
        
        public Dictionary<string, Cell> Sea;

        public void InitSea()
        {
            Sea = new Dictionary<string, Cell>();
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    Sea.Add(string.Format("{0}{1}", i, j), new Cell(i, j));    
                }
            }
        }

        public void CalculateValues()
        {
            try
            {

            }
            catch
            {
                foreach (var cell in Sea.Values)
                {
                    cell.Value = 10;
                }
            }
        }

        public void InitShips()
        {
            var shipSetup1 = new ShipSetup(
                new List<Ship>()
                {
                    new Ship() { Forma = Forma.F4, Kryptis = Kryptis.D, Koordinates = "b10", X = 2, Y = 10 },
                    new Ship() { Forma = Forma.F3, Kryptis = Kryptis.Z, Koordinates = "j5", X = 10, Y = 5},
                    new Ship() { Forma = Forma.F3, Kryptis = Kryptis.D, Koordinates = "h9", X = 8, Y = 9},
                    new Ship() { Forma = Forma.F2, Kryptis = Kryptis.Z, Koordinates = "j1", X = 10, Y = 1},
                    new Ship() { Forma = Forma.F2, Kryptis = Kryptis.D, Koordinates = "c1", X = 3, Y = 1},
                    new Ship() { Forma = Forma.F2, Kryptis = Kryptis.Z, Koordinates = "b3", X = 2, Y = 3},
                    new Ship() { Forma = Forma.F1, Kryptis = Kryptis.Z, Koordinates = "c7", X = 3, Y = 7},
                    new Ship() { Forma = Forma.F1, Kryptis = Kryptis.Z, Koordinates = "g6", X = 7, Y = 6},
                    new Ship() { Forma = Forma.F1, Kryptis = Kryptis.Z, Koordinates = "f8", X = 8, Y = 8},
                    new Ship() { Forma = Forma.F1, Kryptis = Kryptis.Z, Koordinates = "f2", X = 8, Y = 2},
                });
            foreach (var ship in shipSetup1.Setup)
            {
                //todo atspausdint ir sudėt į jūrą
            }
            
        }
    }
}
