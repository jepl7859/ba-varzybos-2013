﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EpicBattleship
{
    public partial class AlternativeInterface : Form
    {
        private readonly GameEngine _engine = new GameEngine();

        public AlternativeInterface()
        {
            InitializeComponent();
            ShipPositionsGroup.Hide();
            OurMoveGroup.Hide();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            try
            {
                StartButton.Enabled = false;
                ShipArrangementBox.Text = _engine.InitGameBoard();
                ShipArrangementBox.ReadOnly = true;

                DisableResultButtons();

                ShipPositionsGroup.Show();
                OurMoveGroup.Show();
            }
            catch
            {
                ShipArrangementBox.Text = "Pasiduodam :(";
            }
        }

        private void NextMoveButton_Click(object sender, EventArgs e)
        {
            EnableResultButtons();
            NextMoveOutput.Text = _engine.MakeMove();
            Tikimybes.Text = _engine.OpponentGameBoard.ToString();
            History.Text += "Šaunam: "+NextMoveOutput.Text + "\n";
        }

        private void MissButton_Click(object sender, EventArgs e)
        {
            DisableResultButtons();
            _engine.InsertMoveResults(MoveResult.Miss);
        }

        private void HitButton_Click(object sender, EventArgs e)
        {
            DisableResultButtons();
            _engine.InsertMoveResults(MoveResult.Hit);
        }

        private void DeadButton_Click(object sender, EventArgs e)
        {
            DisableResultButtons();
            int killed = _engine.InsertMoveResults(MoveResult.Dead);
            if (killed > 0)
            {
                switch (killed)
                {
                    case 1:
                        txt1F.Text = (Convert.ToInt32(txt1F.Text) - 1).ToString();
                        break;
                    case 2:
                        txt2F.Text = (Convert.ToInt32(txt2F.Text) - 1).ToString();
                        break;
                    case 3:
                        txt3F.Text = (Convert.ToInt32(txt3F.Text) - 1).ToString();
                        break;
                    case 4:
                        txt4F.Text = (Convert.ToInt32(txt4F.Text) - 1).ToString();
                        break;
                }
            }
        }

        private void DisableResultButtons()
        {
            NextMoveButton.Enabled = true;
            ShipMovement.Enabled = true;
            MissButton.Enabled = false;
            HitButton.Enabled = false;
            DeadButton.Enabled = false;
        }

        private void EnableResultButtons()
        {
            NextMoveButton.Enabled = false;
            ShipMovement.Enabled = false;
            MissButton.Enabled = true;
            HitButton.Enabled = true;
            DeadButton.Enabled = true;
        }

        private void ShipMovement_Click(object sender, EventArgs e)
        {
            _engine.InsertOpponentMove(MoveType.ShipPush);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
