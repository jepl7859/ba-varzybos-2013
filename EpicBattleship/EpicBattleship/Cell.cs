﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpicBattleship
{
    public class Cell
    {
        public List<string> Letters = new List<string>() { "","A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        public int X;
        public int Y;
        public string Key;

        public CellState State;
        public int Value;

        public Cell(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Key = string.Format("{0}{1}", Letters[x], y);
            State = CellState.Clear;
            Value = 0;
        }
    }

    
    public enum CellState
    {
        Clear,
        Hit,
        Miss,
        NearHit,
        Ship
    }
}
