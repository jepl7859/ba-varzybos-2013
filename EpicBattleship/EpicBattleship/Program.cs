﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EpicBattleship
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var alternativeInterface = Boolean.Parse(System.Configuration.ConfigurationSettings.AppSettings["AlternativeInterface"]);

            if (alternativeInterface)
            {
                Application.Run(new AlternativeInterface());
            }
            else
            {
                Application.Run(new Form1());
            }
        }
    }
}
