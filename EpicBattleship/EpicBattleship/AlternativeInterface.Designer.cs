﻿namespace EpicBattleship
{
    partial class AlternativeInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NextMoveOutput = new System.Windows.Forms.Label();
            this.NextMoveButton = new System.Windows.Forms.Button();
            this.MissButton = new System.Windows.Forms.Button();
            this.HitButton = new System.Windows.Forms.Button();
            this.DeadButton = new System.Windows.Forms.Button();
            this.OurMoveGroup = new System.Windows.Forms.GroupBox();
            this.ShipMovement = new System.Windows.Forms.Button();
            this.ShipArrangementBox = new System.Windows.Forms.RichTextBox();
            this.ShipPositionsGroup = new System.Windows.Forms.GroupBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.History = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Tikimybes = new System.Windows.Forms.RichTextBox();
            this.LikoLabel = new System.Windows.Forms.Label();
            this.lbl4F = new System.Windows.Forms.Label();
            this.txt4F = new System.Windows.Forms.Label();
            this.lbl3F = new System.Windows.Forms.Label();
            this.txt3F = new System.Windows.Forms.Label();
            this.lbl2F = new System.Windows.Forms.Label();
            this.txt2F = new System.Windows.Forms.Label();
            this.lbl1F = new System.Windows.Forms.Label();
            this.txt1F = new System.Windows.Forms.Label();
            this.OurMoveGroup.SuspendLayout();
            this.ShipPositionsGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kitas ėjimas";
            // 
            // NextMoveOutput
            // 
            this.NextMoveOutput.AutoSize = true;
            this.NextMoveOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.NextMoveOutput.Location = new System.Drawing.Point(77, 20);
            this.NextMoveOutput.Name = "NextMoveOutput";
            this.NextMoveOutput.Size = new System.Drawing.Size(19, 26);
            this.NextMoveOutput.TabIndex = 2;
            this.NextMoveOutput.Text = "-";
            // 
            // NextMoveButton
            // 
            this.NextMoveButton.Location = new System.Drawing.Point(12, 59);
            this.NextMoveButton.Name = "NextMoveButton";
            this.NextMoveButton.Size = new System.Drawing.Size(75, 23);
            this.NextMoveButton.TabIndex = 3;
            this.NextMoveButton.Text = "Kitas ėjimas";
            this.NextMoveButton.UseVisualStyleBackColor = true;
            this.NextMoveButton.Click += new System.EventHandler(this.NextMoveButton_Click);
            // 
            // MissButton
            // 
            this.MissButton.Location = new System.Drawing.Point(12, 97);
            this.MissButton.Name = "MissButton";
            this.MissButton.Size = new System.Drawing.Size(75, 23);
            this.MissButton.TabIndex = 4;
            this.MissButton.Text = "Nepataikė";
            this.MissButton.UseVisualStyleBackColor = true;
            this.MissButton.Click += new System.EventHandler(this.MissButton_Click);
            // 
            // HitButton
            // 
            this.HitButton.Location = new System.Drawing.Point(93, 97);
            this.HitButton.Name = "HitButton";
            this.HitButton.Size = new System.Drawing.Size(75, 23);
            this.HitButton.TabIndex = 5;
            this.HitButton.Text = "Pataikė";
            this.HitButton.UseVisualStyleBackColor = true;
            this.HitButton.Click += new System.EventHandler(this.HitButton_Click);
            // 
            // DeadButton
            // 
            this.DeadButton.Location = new System.Drawing.Point(174, 97);
            this.DeadButton.Name = "DeadButton";
            this.DeadButton.Size = new System.Drawing.Size(75, 23);
            this.DeadButton.TabIndex = 6;
            this.DeadButton.Text = "Numušė";
            this.DeadButton.UseVisualStyleBackColor = true;
            this.DeadButton.Click += new System.EventHandler(this.DeadButton_Click);
            // 
            // OurMoveGroup
            // 
            this.OurMoveGroup.Controls.Add(this.ShipMovement);
            this.OurMoveGroup.Controls.Add(this.MissButton);
            this.OurMoveGroup.Controls.Add(this.NextMoveButton);
            this.OurMoveGroup.Controls.Add(this.DeadButton);
            this.OurMoveGroup.Controls.Add(this.NextMoveOutput);
            this.OurMoveGroup.Controls.Add(this.HitButton);
            this.OurMoveGroup.Controls.Add(this.label1);
            this.OurMoveGroup.Location = new System.Drawing.Point(21, 277);
            this.OurMoveGroup.Name = "OurMoveGroup";
            this.OurMoveGroup.Size = new System.Drawing.Size(260, 126);
            this.OurMoveGroup.TabIndex = 7;
            this.OurMoveGroup.TabStop = false;
            this.OurMoveGroup.Text = "Mūsų ėjimai";
            // 
            // ShipMovement
            // 
            this.ShipMovement.Location = new System.Drawing.Point(93, 59);
            this.ShipMovement.Name = "ShipMovement";
            this.ShipMovement.Size = new System.Drawing.Size(75, 23);
            this.ShipMovement.TabIndex = 7;
            this.ShipMovement.Text = "Pastumė";
            this.ShipMovement.UseVisualStyleBackColor = true;
            this.ShipMovement.Click += new System.EventHandler(this.ShipMovement_Click);
            // 
            // ShipArrangementBox
            // 
            this.ShipArrangementBox.Location = new System.Drawing.Point(12, 19);
            this.ShipArrangementBox.Name = "ShipArrangementBox";
            this.ShipArrangementBox.Size = new System.Drawing.Size(237, 198);
            this.ShipArrangementBox.TabIndex = 9;
            this.ShipArrangementBox.Text = "";
            // 
            // ShipPositionsGroup
            // 
            this.ShipPositionsGroup.Controls.Add(this.ShipArrangementBox);
            this.ShipPositionsGroup.Location = new System.Drawing.Point(21, 48);
            this.ShipPositionsGroup.Name = "ShipPositionsGroup";
            this.ShipPositionsGroup.Size = new System.Drawing.Size(260, 223);
            this.ShipPositionsGroup.TabIndex = 10;
            this.ShipPositionsGroup.TabStop = false;
            this.ShipPositionsGroup.Text = "Laivų išdėstymas";
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(33, 13);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(248, 23);
            this.StartButton.TabIndex = 11;
            this.StartButton.Text = "Pradėti žaidimą";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.History);
            this.groupBox1.Location = new System.Drawing.Point(287, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(165, 390);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Istorija";
            // 
            // History
            // 
            this.History.Location = new System.Drawing.Point(12, 19);
            this.History.Name = "History";
            this.History.ReadOnly = true;
            this.History.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.History.Size = new System.Drawing.Size(147, 365);
            this.History.TabIndex = 9;
            this.History.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt1F);
            this.groupBox2.Controls.Add(this.lbl1F);
            this.groupBox2.Controls.Add(this.txt2F);
            this.groupBox2.Controls.Add(this.lbl2F);
            this.groupBox2.Controls.Add(this.txt3F);
            this.groupBox2.Controls.Add(this.lbl3F);
            this.groupBox2.Controls.Add(this.txt4F);
            this.groupBox2.Controls.Add(this.lbl4F);
            this.groupBox2.Controls.Add(this.LikoLabel);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.Tikimybes);
            this.groupBox2.Location = new System.Drawing.Point(467, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(348, 393);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tikimybės";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(6, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 180);
            this.label3.TabIndex = 11;
            this.label3.Text = "1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n10\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(37, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(298, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = " A  B  C  D  E  F  G  H  I  J";
            // 
            // Tikimybes
            // 
            this.Tikimybes.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Tikimybes.Location = new System.Drawing.Point(40, 46);
            this.Tikimybes.Name = "Tikimybes";
            this.Tikimybes.ReadOnly = true;
            this.Tikimybes.Size = new System.Drawing.Size(302, 326);
            this.Tikimybes.TabIndex = 9;
            this.Tikimybes.Text = "";
            // 
            // LikoLabel
            // 
            this.LikoLabel.AutoSize = true;
            this.LikoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.LikoLabel.Location = new System.Drawing.Point(37, 377);
            this.LikoLabel.Name = "LikoLabel";
            this.LikoLabel.Size = new System.Drawing.Size(35, 13);
            this.LikoLabel.TabIndex = 12;
            this.LikoLabel.Text = "Liko:";
            // 
            // lbl4F
            // 
            this.lbl4F.AutoSize = true;
            this.lbl4F.Location = new System.Drawing.Point(79, 377);
            this.lbl4F.Name = "lbl4F";
            this.lbl4F.Size = new System.Drawing.Size(22, 13);
            this.lbl4F.TabIndex = 13;
            this.lbl4F.Text = "4F:";
            // 
            // txt4F
            // 
            this.txt4F.AutoSize = true;
            this.txt4F.Location = new System.Drawing.Point(96, 377);
            this.txt4F.Name = "txt4F";
            this.txt4F.Size = new System.Drawing.Size(13, 13);
            this.txt4F.TabIndex = 14;
            this.txt4F.Text = "1";
            // 
            // lbl3F
            // 
            this.lbl3F.AutoSize = true;
            this.lbl3F.Location = new System.Drawing.Point(115, 377);
            this.lbl3F.Name = "lbl3F";
            this.lbl3F.Size = new System.Drawing.Size(22, 13);
            this.lbl3F.TabIndex = 15;
            this.lbl3F.Text = "3F:";
            this.lbl3F.Click += new System.EventHandler(this.label4_Click);
            // 
            // txt3F
            // 
            this.txt3F.AutoSize = true;
            this.txt3F.Location = new System.Drawing.Point(134, 377);
            this.txt3F.Name = "txt3F";
            this.txt3F.Size = new System.Drawing.Size(13, 13);
            this.txt3F.TabIndex = 16;
            this.txt3F.Text = "2";
            // 
            // lbl2F
            // 
            this.lbl2F.AutoSize = true;
            this.lbl2F.Location = new System.Drawing.Point(153, 377);
            this.lbl2F.Name = "lbl2F";
            this.lbl2F.Size = new System.Drawing.Size(22, 13);
            this.lbl2F.TabIndex = 17;
            this.lbl2F.Text = "2F:";
            // 
            // txt2F
            // 
            this.txt2F.AutoSize = true;
            this.txt2F.Location = new System.Drawing.Point(173, 377);
            this.txt2F.Name = "txt2F";
            this.txt2F.Size = new System.Drawing.Size(13, 13);
            this.txt2F.TabIndex = 18;
            this.txt2F.Text = "3";
            // 
            // lbl1F
            // 
            this.lbl1F.AutoSize = true;
            this.lbl1F.Location = new System.Drawing.Point(192, 377);
            this.lbl1F.Name = "lbl1F";
            this.lbl1F.Size = new System.Drawing.Size(22, 13);
            this.lbl1F.TabIndex = 19;
            this.lbl1F.Text = "1F:";
            // 
            // txt1F
            // 
            this.txt1F.AutoSize = true;
            this.txt1F.Location = new System.Drawing.Point(210, 377);
            this.txt1F.Name = "txt1F";
            this.txt1F.Size = new System.Drawing.Size(13, 13);
            this.txt1F.TabIndex = 20;
            this.txt1F.Text = "4";
            // 
            // AlternativeInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 426);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.ShipPositionsGroup);
            this.Controls.Add(this.OurMoveGroup);
            this.MinimumSize = new System.Drawing.Size(836, 441);
            this.Name = "AlternativeInterface";
            this.Text = "Randomizers FTW";
            this.OurMoveGroup.ResumeLayout(false);
            this.OurMoveGroup.PerformLayout();
            this.ShipPositionsGroup.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label NextMoveOutput;
        private System.Windows.Forms.Button NextMoveButton;
        private System.Windows.Forms.Button MissButton;
        private System.Windows.Forms.Button HitButton;
        private System.Windows.Forms.Button DeadButton;
        private System.Windows.Forms.GroupBox OurMoveGroup;
        private System.Windows.Forms.RichTextBox ShipArrangementBox;
        private System.Windows.Forms.GroupBox ShipPositionsGroup;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Button ShipMovement;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox History;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox Tikimybes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl3F;
        private System.Windows.Forms.Label txt4F;
        private System.Windows.Forms.Label lbl4F;
        private System.Windows.Forms.Label LikoLabel;
        private System.Windows.Forms.Label txt2F;
        private System.Windows.Forms.Label lbl2F;
        private System.Windows.Forms.Label txt3F;
        private System.Windows.Forms.Label txt1F;
        private System.Windows.Forms.Label lbl1F;
    }
}