﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EpicBattleship
{
    public class GameEngine
    {

        public GameBoard GameBaord;
        public GameBoard OpponentGameBoard;
        public List<Cell> Moves;
        public List<Cell> Hits; 

        public string InitGameBoard()
        {
            GameBaord = new GameBoard();
            OpponentGameBoard = new GameBoard();
            Moves = new List<Cell>();
            Hits = new List<Cell>();

            GameBaord.InitSea();
            OpponentGameBoard.InitSea();
            OpponentGameBoard.InitShips(true);

            return GameBaord.InitShips();
        }

        public string MakeMove()
        {
            OpponentGameBoard.CalculateValues();

            var cellToShoot = OpponentGameBoard.GetCellWithBestProbability();

            if (cellToShoot.State != CellState.Clear )
            {
                cellToShoot = OpponentGameBoard.GetRandomCell();
            }

            Moves.Add(cellToShoot);

            return cellToShoot.Key;
        }

        public int InsertMoveResults(MoveResult moveResult)
        {
            var ret = 0;
            var lastMove = Moves.Last();
            switch (moveResult)
            {
                case MoveResult.Miss:
                    lastMove.State = CellState.Miss;
                    break;
                case MoveResult.Hit:
                    lastMove.State = CellState.Hit;
                    Hits.Add(lastMove);
                    OpponentGameBoard.MarkCornerCellsNearKill(lastMove);
                    break;
                case MoveResult.Dead:
                    lastMove.State = CellState.Hit;
                    Hits.Add(lastMove);
                    OpponentGameBoard.MarkAllAroundDeadShip(Hits);
                    OpponentGameBoard.RemoveKilledShip(Hits);
                    ret = Hits.Count;
                    Hits.Clear();
                    break;
                case MoveResult.SecondShot:
                    lastMove.State = CellState.Hit;
                    break;
            }
            return ret;
        }

        public void InsertOpponentMove(MoveType moveType, string move = "")
        {
            switch (moveType)
            {
                case MoveType.Shoot:
                    break;
                case MoveType.ShipPush:
                    OpponentGameBoard.ResetMissedCells();
                    break;
            }
        }

        public bool RepetetiveMove()
        {
            try
            {
                var lastMoveIndex = Moves.Count - 1 >= 0 ? Moves.Count - 1 : 0;
                var beforeLastMoveIndex = lastMoveIndex - 1 >= 0 ? lastMoveIndex - 1 : 0;

                if (Moves.Any() && beforeLastMoveIndex <= lastMoveIndex && Moves[beforeLastMoveIndex].Key != Moves[lastMoveIndex].Key)
                {
                    return false;
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }
    }

    public enum MoveType
    {
        Shoot,
        ShipPush
    }

    public enum MoveResult
    {
        Hit,
        Miss,
        Dead,
        SecondShot
    }
}
